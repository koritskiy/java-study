package com.example.study.controllers;


import com.example.study.dao.RequestDao;
import com.example.study.models.Client;
import com.example.study.models.Employer;
import com.example.study.models.Request;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(value = "/api", produces = MediaType.APPLICATION_JSON_VALUE)
public class RequestController {

    @Autowired
    private RequestDao requestDao;

    @GetMapping("/requests")
    public List<Request> getAllRequests(){
        return requestDao.getAll();
    }

    @GetMapping("/request/{requestId}")
    public Request getById(@PathVariable("requestId") int id){
        return requestDao.getById(id);
    }

    @PostMapping("/requests")
    @ResponseStatus(HttpStatus.CREATED)
    public Request createRequest(@RequestBody Request request) {
        return requestDao.createRequest(request);
    }
}
