package com.example.study.dao;

import com.example.study.models.Client;
import com.example.study.models.Employer;
import com.example.study.models.Request;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;
import java.util.List;

@Repository
@Transactional
public class RequestDao {

    // Менеджер для манипуляции с сущностями бд
    @PersistenceContext
    private EntityManager entityManager;

    // Метод для запроса к бд, вытягивающий все заявки
    public List<Request> getAll(){
        return entityManager.createQuery("from Request c order by c.id desc", Request.class).getResultList();
    }

    // Метод для поиска заявки по id
    public Request getById(int id){
        return entityManager.find(Request.class, id);
    }

    // Метод создания заявки
    public Request createRequest(Request request){
        Employer employer = request.getClient().getEmployer();
        Client client = request.getClient();

        // employer.setClient(client);
        // client.setRequest(request);
        entityManager.persist(request);
        return request;
    }
}
