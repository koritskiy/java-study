package com.example.study.models;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.example.study.models.Employer;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
public class Client {

    // ID заемщика
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    // Имя пользователя
    private String name;

    // Дата рождения
    // @JsonFormat(pattern="yyyy-MM-dd")
    private String birthday;

    // Зарплата пользователя
    private int salary;

    // Расходы пользователя
    private int outlay;

    // Работодатель
    // @OneToOne(mappedBy = "client", cascade = CascadeType.ALL)
    @OneToOne(mappedBy = "client", cascade = CascadeType.ALL, orphanRemoval = true)
    private Employer employer;

    // Это для сохранения и реализации бд. К клиенту присоединяем его работу
    public void setEmployer(Employer employer){
        this.employer = employer;
    }

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "request_id")
    private Request request;

    public Request getRequest() {
        return request;
    }

    public void setRequest(Request request) {
        this.request = request;
    }

    public Employer getEmployer(){
        return employer;
    }
}
