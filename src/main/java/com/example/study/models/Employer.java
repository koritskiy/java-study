package com.example.study.models;

import javax.persistence.*;
import javax.validation.constraints.Pattern;

@Entity
public class Employer {

    // ID работодателя
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    // ИНН работодателя
    // @Pattern(regexp = "[0-9]{10}")
    private int INN;

    // Названия работодателя
    private String name;

    // Телефон компании
    // @Pattern(regexp = "(\\+7)[0-9]{9}")
    private int phoneNumber;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "client_id")
    private Client client;

    public Client getClient() {
        return client;
    }

    public void setClient(Client client) {
        this.client = client;
    }
}
