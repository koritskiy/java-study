package com.example.study.models;


import com.fasterxml.jackson.annotation.JsonFormat;

import javax.persistence.*;
import java.lang.ref.Cleaner;
import java.time.LocalDateTime;

@Entity
public class Request {

    // ID заявки
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    // Дата заявки
    // @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss")
    private String requestDate;

    // Сумма кредита
    private int sum;

    // Срок кредита
    private int period;

    // Заемщик
    @OneToOne(mappedBy = "request", cascade = CascadeType.ALL, orphanRemoval = true)
    // @RestResource(path = "libraryAddress", rel="address")
    private Client client;

    // Это для присоединения клиента к заявке
    public void setClient(Client client){
        this.client = client;
    }

    public Client getClient(){
        return client;
    }
}
